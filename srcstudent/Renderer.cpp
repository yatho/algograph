#include <Renderer.h>
#include <Panel3D.h>
#include <Engine.h>

void Renderer::DrawFilaire()
{
    for (int indFace = 0; indFace < (drawable->faces).size; indFace++) {
        Face & face = (drawable->faces).data[indFace];
        Color color1, color2, color3;

        if (drawable->colorOnFace == true) {
            //Si on est en mode kaléidoscope
            color1 = (drawable->faceColors).data[indFace];
            color2 = color1;
            color3 = color1;
        }
        else {
            Array<Color> & colors = (drawable->pointColors);
            color1 = colors.data[face.index1];
            color2 = colors.data[face.index2];
            color3 = colors.data[face.index3];
        }

        //Dessin du triangle
        buffer->DrawLine(renderable.points2D.data[face.index1], renderable.points2D.data[face.index2], color1, color2);
        buffer->DrawLine(renderable.points2D.data[face.index2], renderable.points2D.data[face.index3], color2, color3);
        buffer->DrawLine(renderable.points2D.data[face.index3], renderable.points2D.data[face.index1], color3, color1);
    }
}
void Renderer::DrawFilaireCache()
{
	for (int indFace = 0; indFace < (drawable->faces).size; indFace++) {
        if ((effectiveDrawable->faceVisibles).data[indFace] == true) {
            //Si la face est visible

            Face & face = (drawable->faces).data[indFace];
            Color color1, color2, color3;

            if (drawable->colorOnFace == true) {
                //Si on est en mode kaléidoscope
                color1 = (drawable->faceColors).data[indFace];
                color2 = color1;
                color3 = color1;
            }
            else {
                Array<Color> & colors = (drawable->pointColors);
                color1 = colors.data[face.index1];
                color2 = colors.data[face.index2];
                color3 = colors.data[face.index3];
            }

            //Dessin du triangle
            buffer->DrawLine(renderable.points2D.data[face.index1], renderable.points2D.data[face.index2], color1, color2);
            buffer->DrawLine(renderable.points2D.data[face.index2], renderable.points2D.data[face.index3], color2, color3);
            buffer->DrawLine(renderable.points2D.data[face.index3], renderable.points2D.data[face.index1], color3, color1);
        }
    }
}

void Renderer::DrawFacePleine()
{
	for (int indFace = 0; indFace < (drawable->faces).size; indFace++) {
        if ((effectiveDrawable->faceVisibles).data[indFace] == true) {
            //Si la face est visible

            Face & face = (drawable->faces).data[indFace];
            Color color1, color2, color3;

            if (drawable->colorOnFace == true) {
                //Si on est en mode kaléidoscope
                color1 = (drawable->faceColors).data[indFace];
                color2 = color1;
                color3 = color1;
            }
            else {
                Array<Color> & colors = drawable->pointColors;
                color1 = colors.data[face.index1];
                color2 = colors.data[face.index2];
                color3 = colors.data[face.index3];
            }

            //Remplissage de la face
            buffer->DrawFilledTriangle(renderable.points2D.data[face.index1],
                renderable.points2D.data[face.index2],
                renderable.points2D.data[face.index3],
                color1, color2, color3);
        }
	}
}

void Renderer::DrawLambert()
{
    for (int numFace = 0; numFace < effectiveDrawable->sortedVisibleFaces.size; numFace++) {
        int indFace = effectiveDrawable->sortedVisibleFaces.data[numFace].index;
        Face & face = (drawable->faces).data[indFace];
        Color color1, color2, color3;

        //Coordonnées 3D de chaque sommet du triangle
        Coord3D & p1((effectiveDrawable->points).data[face.index1]);
        Coord3D & p2((effectiveDrawable->points).data[face.index2]);
        Coord3D & p3((effectiveDrawable->points).data[face.index3]);

        //Coordonnées 3D du point central de la face
        Coord3D ptMilieu((p1.x + p2.x + p3.x) / 3, (p1.y + p2.y + p3.y) / 3, (p1.z + p2.z + p3.z) / 3);

        //Lumière ambiante reçue par le point central
        Color colorLight = pointLight.GetColor(ptMilieu, effectiveDrawable->faceNormals.data[indFace]);

        if (drawable->colorOnFace == true) {
            //Si on est en mode kaléidoscope
            color1 = (drawable->faceColors).data[indFace] * (ambientLight.ambientColor + colorLight);
            color2 = color1;
            color3 = color1;
        }
        else {
            Array<Color> & colors = drawable->pointColors;
            color1 = colors.data[face.index1] * (ambientLight.ambientColor + colorLight);
            color2 = colors.data[face.index2] * (ambientLight.ambientColor + colorLight);
            color3 = colors.data[face.index3] * (ambientLight.ambientColor + colorLight);
        }

        //Remplissage de la face
        buffer->DrawFilledTriangle(renderable.points2D.data[face.index1],
            renderable.points2D.data[face.index2],
            renderable.points2D.data[face.index3],
            color1, color2, color3);
	}
}


void Renderer::DrawGouraud()
{
	for (int numFace = 0; numFace < effectiveDrawable->sortedVisibleFaces.size; numFace++) {
        int indFace = effectiveDrawable->sortedVisibleFaces.data[numFace].index;
        Face & face = (drawable->faces).data[indFace];
        Color color1, color2, color3;

        //Coordonnées 3D des 3 sommets formant le triangle
        Coord3D & p1((effectiveDrawable->points).data[face.index1]);
        Coord3D & p2((effectiveDrawable->points).data[face.index2]);
        Coord3D & p3((effectiveDrawable->points).data[face.index3]);

        //Lumière ponctuelle reçue par chacun des sommets du triangle
        Color colorLight1 = pointLight.GetColor(p1, effectiveDrawable->pointNormals.data[face.index1]);
        Color colorLight2 = pointLight.GetColor(p2, effectiveDrawable->pointNormals.data[face.index2]);
        Color colorLight3 = pointLight.GetColor(p3, effectiveDrawable->pointNormals.data[face.index3]);

        if (drawable->colorOnFace == true) {
            //Si on est en mode kaléidoscope
            color1 = (drawable->faceColors).data[indFace] * (ambientLight.ambientColor + colorLight1);
            color2 = (drawable->faceColors).data[indFace] * (ambientLight.ambientColor + colorLight2);
            color3 = (drawable->faceColors).data[indFace] * (ambientLight.ambientColor + colorLight3);
        }
        else {
            Array<Color> & colors = drawable->pointColors;
            color1 = colors.data[face.index1] * (ambientLight.ambientColor + colorLight1);
            color2 = colors.data[face.index2] * (ambientLight.ambientColor + colorLight2);
            color3 = colors.data[face.index3] * (ambientLight.ambientColor + colorLight3);
        }

        //Remplissage de la face
        buffer->DrawFilledTriangle(renderable.points2D.data[face.index1],
            renderable.points2D.data[face.index2],
            renderable.points2D.data[face.index3],
            color1, color2, color3);
	}
}


void Renderer::DrawPhong()
{
	for (int numFace = 0; numFace < effectiveDrawable->sortedVisibleFaces.size; numFace++) {
        int indFace = effectiveDrawable->sortedVisibleFaces.data[numFace].index;
        Face & face = (drawable->faces).data[indFace];
        Color color1, color2, color3;

        //Coordonnées 3D des 3 sommets formant le triangle
        Coord3D & p1((effectiveDrawable->points).data[face.index1]);
        Coord3D & p2((effectiveDrawable->points).data[face.index2]);
        Coord3D & p3((effectiveDrawable->points).data[face.index3]);

        //Coordonnées 3D des normales des 3 sommets formant le triangle
        Coord3D & normale1((effectiveDrawable->pointNormals).data[face.index1]);
        Coord3D & normale2((effectiveDrawable->pointNormals).data[face.index2]);
        Coord3D & normale3((effectiveDrawable->pointNormals).data[face.index3]);

        if (drawable->colorOnFace == true) {
            //Si on est en mode kaléidoscope
            color1 = (drawable->faceColors).data[indFace];
            color2 = color1;
            color3 = color2;
        }
        else {
            Array<Color> & colors = drawable->pointColors;
            color1 = colors.data[face.index1];
            color2 = colors.data[face.index2];
            color3 = colors.data[face.index3];
        }

        //Remplissage de la face
        buffer->DrawPhongTriangle(renderable.points2D.data[face.index1],
            renderable.points2D.data[face.index2],
            renderable.points2D.data[face.index3],
            color1, color2, color3,
            p1, p2, p3, normale1, normale2, normale3,
            ambientLight, pointLight);
	}
}
