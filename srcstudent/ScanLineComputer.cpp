
#include <ScanLineComputer.h>
#include <Buffer.h>
#include <FastMath.h>
#include <limits.h>



void ScanLineComputer::AddEdge(const Coord2D p1, const Coord2D p2,
															 const int index1, const int index2)
{
	// On utilise la méthode de Bresenheim pour calculer les points du segment [p1,p2] et on les ajoute via la méthode AddPoint
	Coord2D point, longPoint;
	int critere, const1, const2;
	Coord2D inc;
	int compteur;

    //Initialisation
	point.x = p1.x;
	point.y = p1.y;

	longPoint.x = p2.x - p1.x;
	longPoint.y = p2.y - p1.y;

	if (longPoint.x >= 0) {
        inc.x = 1;
	}
	else {
        inc.x = -1;
        longPoint.x = - longPoint.x;
    }

    if (longPoint.y >= 0) {
        inc.y = 1;
    }
    else {
        inc.y = -1;
        longPoint.y = -longPoint.y;
    }

    if (longPoint.y < longPoint.x) {
        const1 = 2*(longPoint.y - longPoint.x);
        const2 = 2*longPoint.y;
        critere = const2 - longPoint.x;
        for (compteur = 1; compteur <= longPoint.x ; compteur++) {
            if ((point.y >= 0) && (point.y < height)) {
                //Ajout du point
                AddPoint(point.x, point.y, p1, p2, index1, index2);
            }

            if (critere > 0) {
                point.y += inc.y;
                critere += const1;
            }
            else {
                critere += const2;
            }
            point.x += inc.x;
        }
    }
    else {
        const1 = 2*(longPoint.x - longPoint.y);
        const2 = 2*longPoint.x;
        critere = const2 - longPoint.y;
        for (compteur = 1; compteur <= longPoint.y; compteur++) {
            if ((point.y >= 0) && (point.y < height)) {
                //Ajout du point
                AddPoint(point.x, point.y, p1, p2, index1, index2);
            }

            if (critere > 0) {
                point.x += inc.x;
                critere += const1;
            }
            else {
                critere += const2;
            }
            point.y += inc.y;
        }
    }
}


