#include <Buffer.h>
#include <FastMath.h>


void Buffer::DrawLine(const Coord2D p1, const Coord2D p2, const Color c1,
		const Color c2)
{
	Coord2D point, longPoint;
	int critere, const1, const2;
	Coord2D inc;
	int compteur;
	double wa, wb;

    //Initialisation
	point.x = p1.x;
	point.y = p1.y;
	point.depth = p1.depth;

	longPoint.x = p2.x - p1.x;
	longPoint.y = p2.y - p1.y;

	if (longPoint.x >= 0) {
        inc.x = 1;
	}
	else {
        inc.x = -1;
        longPoint.x = - longPoint.x;
    }

    if (longPoint.y >= 0) {
        inc.y = 1;
    }
    else {
        inc.y = -1;
        longPoint.y = -longPoint.y;
    }

    if (longPoint.y < longPoint.x) {
        const1 = 2*(longPoint.y - longPoint.x);
        const2 = 2*longPoint.y;
        critere = const2 - longPoint.x;

        for (compteur = 1; compteur <= longPoint.x ; compteur++) {
            //Calcul des poids
            wa = 1 - (p1.Distance(point))/(p1.Distance(p2));
            wb = 1-wa;
            point.depth = p1.depth * wa + p2.depth * wb;

            //Affichage du point
            SetPoint(point, c1*wa + c2*wb);

            if (critere > 0) {
                point.y += inc.y;
                critere += const1;
            }
            else {
                critere += const2;
            }
            point.x += inc.x;
        }
    }
    else {
        const1 = 2*(longPoint.x - longPoint.y);
        const2 = 2*longPoint.x;
        critere = const2 - longPoint.y;

        for (compteur = 1; compteur <= longPoint.y; compteur++) {
            //Calcul des poids
            wa = 1 - (p1.Distance(point))/(p1.Distance(p2));
            wb = 1-wa;
            point.depth = p1.depth * wa + p2.depth * wb;

            //Affichage du point
            SetPoint(point, c1*wa + c2*wb);

            if (critere > 0) {
                point.x += inc.x;
                critere += const1;
            }
            else {
                critere += const2;
            }
            point.y += inc.y;
        }
    }
}

void Buffer::DrawFilledTriangle(const Coord2D p1, const Coord2D p2,
		const Coord2D p3, const Color c1, const Color c2, const Color c3)
{
    //Calcul des points formant les bordures du triangle
    scanLineComputer.Init();
	scanLineComputer.Compute(p1, p2, p3);

    for (int ordY = scanLineComputer.ymin; ordY <= scanLineComputer.ymax; ordY++) {
        Coord2D ptGauche(scanLineComputer.left.data[ordY], ordY);
        Coord2D ptDroite(scanLineComputer.right.data[ordY], ordY);

        //Calcul de la profondeur des deux points extrêmes formant la ligne
        ptGauche.depth = (p1.depth * scanLineComputer.leftweight.data[ordY].data[0])
            + (p2.depth * scanLineComputer.leftweight.data[ordY].data[1])
            + (p3.depth * scanLineComputer.leftweight.data[ordY].data[2]);

        ptDroite.depth = (p1.depth * scanLineComputer.rightweight.data[ordY].data[0])
            + (p2.depth * scanLineComputer.rightweight.data[ordY].data[1])
            + (p3.depth * scanLineComputer.rightweight.data[ordY].data[2]);

        //Calcul de la couleur des deux points extrêmes formant la ligne
        Color colGauche((c1 * scanLineComputer.leftweight.data[ordY].data[0])
            + (c2 * scanLineComputer.leftweight.data[ordY].data[1])
            + (c3 * scanLineComputer.leftweight.data[ordY].data[2]));
        Color colDroite((c1 * scanLineComputer.rightweight.data[ordY].data[0])
            + (c2 * scanLineComputer.rightweight.data[ordY].data[1])
            + (c3 * scanLineComputer.rightweight.data[ordY].data[2]));

        //Dessin de la ligne
        DrawLine(ptGauche, ptDroite, colGauche, colDroite);
    }

    //Dessin des contours du triangle
    DrawLine(p1, p2, c1, c2);
    DrawLine(p2, p3, c2, c3);
    DrawLine(p3, p1, c3, c1);
}

void Buffer::DrawPhongTriangle(const Coord2D p1, const Coord2D p2,
		const Coord2D p3, const Color c1, const Color c2, const Color c3,
		const Coord3D posi1, const Coord3D posi2, const Coord3D posi3,
		const Coord3D normal1, const Coord3D normal2, const Coord3D normal3,
		const AmbientLight & ambientLight, const PointLight & pointLight)
{
    //Calcul des points formant les bordures du triangle
	scanLineComputer.Init();
	scanLineComputer.Compute(p1, p2, p3);

	for (int ordY = scanLineComputer.ymin; ordY <= scanLineComputer.ymax; ordY++) {
        //Initialisation des poids des trois sommets sur les deux points extrêmes formant la ligne
        double weightGauchep1(scanLineComputer.leftweight.data[ordY].data[0]);
        double weightGauchep2(scanLineComputer.leftweight.data[ordY].data[1]);
        double weightGauchep3(scanLineComputer.leftweight.data[ordY].data[2]);

        double weightDroitep1(scanLineComputer.rightweight.data[ordY].data[0]);
        double weightDroitep2(scanLineComputer.rightweight.data[ordY].data[1]);
        double weightDroitep3(scanLineComputer.rightweight.data[ordY].data[2]);

        //Coordonnées 3D des deux points extrêmes formant la ligne ainsi que leurs normales
        Coord3D ptGauche3D, ptDroite3D, normaleGauche, normaleDroite;

        ptGauche3D += posi1 * weightGauchep1;
        ptGauche3D += posi2 * weightGauchep2;
        ptGauche3D += posi3 * weightGauchep3;

        ptDroite3D += posi1 * weightDroitep1;
        ptDroite3D += posi2 * weightDroitep2;
        ptDroite3D += posi3 * weightDroitep3;

        normaleGauche += normal1 * weightGauchep1;
        normaleGauche += normal2 * weightGauchep2;
        normaleGauche += normal3 * weightGauchep3;

        normaleDroite += normal1 * weightDroitep1;
        normaleDroite += normal2 * weightDroitep2;
        normaleDroite += normal3 * weightDroitep3;


        //Coordonnées 2D des deux points extrêmes formant la ligne
        Coord2D ptGauche(scanLineComputer.left.data[ordY], ordY);
        Coord2D ptDroite(scanLineComputer.right.data[ordY], ordY);

        //Calcul de la profondeur des deux points extrêmes formant la ligne
        ptGauche.depth = (p1.depth * scanLineComputer.leftweight.data[ordY].data[0])
            + (p2.depth * scanLineComputer.leftweight.data[ordY].data[1])
            + (p3.depth * scanLineComputer.leftweight.data[ordY].data[2]);

        ptDroite.depth = (p1.depth * scanLineComputer.rightweight.data[ordY].data[0])
            + (p2.depth * scanLineComputer.rightweight.data[ordY].data[1])
            + (p3.depth * scanLineComputer.rightweight.data[ordY].data[2]);

        //Calcul de la couleur des deux points extrêmes formant la ligne
        Color colGauche((c1 * scanLineComputer.leftweight.data[ordY].data[0])
            + (c2 * scanLineComputer.leftweight.data[ordY].data[1])
            + (c3 * scanLineComputer.leftweight.data[ordY].data[2]));
        Color colDroite((c1 * scanLineComputer.rightweight.data[ordY].data[0])
            + (c2 * scanLineComputer.rightweight.data[ordY].data[1])
            + (c3 * scanLineComputer.rightweight.data[ordY].data[2]));


        for (int ordX = scanLineComputer.left.data[ordY]; ordX <= scanLineComputer.right.data[ordY]; ordX++) {
            //Coordonnées 2D du point en cours de traitement
            Coord2D point(ordX, ordY);

            //Calcul des poids des points extrêmes formant la ligne sur le point en cours de traitement
            double weightLigneG = 1 - (ptGauche.Distance(point)) / (ptGauche.Distance(ptDroite));
            double weightLigneD = 1 - weightLigneG;

            //Calcul de la profondeur du point en cours de traitement
            point.depth = (ptGauche.depth * weightLigneG) + (ptDroite.depth * weightLigneD);

            //Coordonnées 3D du point en cours de traitement et de sa normale
            Coord3D normaleLigne, point3D;

            point3D += ptGauche3D * weightLigneG;
            point3D += ptDroite3D * weightLigneD;

            normaleLigne += normaleGauche * weightLigneG;
            normaleLigne += normaleDroite * weightLigneD;

            //Lumière ponctuelle reçue par le point en cours de traitement
            Color colorLight = pointLight.GetColor(point3D, normaleLigne);

            //Dessin du point
            SetPoint(point, (colGauche * weightLigneG + colDroite * weightLigneD) * (ambientLight.ambientColor + colorLight));
        }

        //Dessin du point le plus à droite
        SetPoint(ptDroite, colDroite * (pointLight.GetColor(ptDroite3D, normaleDroite) + ambientLight.ambientColor));
    }
}

