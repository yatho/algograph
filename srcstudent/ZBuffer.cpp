#include <ZBuffer.h>

/** Initialise le Z-Buffer (les pixels sont à une profondeur infinie initialement) */
void ZBuffer::Init()
{
    //Initialisation des poids de chaque point à +infini
    for (int ordX = 0; ordX < depths.size; ordX++) {
        for (int ordY = 0; ordY < depths.data[ordX].size; ordY++) {
            depths.data[ordX].data[ordY] = INT_MAX;
        }
    }
}

/** Fonction permettant de déterminer si le pixel (p.x,p.y) peut être remplacé par le point p.
	 * @return true si p doit remplacer le point de même coordonnées (p.x,p.y) selon la règle du Z-buffer
	 * i.e. si p est plus proche de la caméra que le même pixel mais de profondeur depths[x][y]. */
bool ZBuffer::ReplaceCurrent(const Coord2D p)
{
    if (this->enabled) {
        if (p.depth <= depths.data[p.y].data[p.x]) {
            //On remplace le point
            depths.data[p.y].data[p.x] = p.depth;
        }
        else {
            return false;
        }
    }
    return true;
}
